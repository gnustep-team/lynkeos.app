lynkeos.app (3.7+dfsg-7) unstable; urgency=medium

  * debian/rules (override_dh_fixperms-indep): Patch the .plist file to
    replace the CFBundleIdentifier with the real string.
  * debian/patches/gnustep-port.patch: Remove workarounds for
    functionality now implemented in GNUstep.  Include the new header
    <CoreFoundation/CFCGTypes.h> provided by gnustep-base.
  * debian/patches/gcc-warnings.patch: Refresh due to the above changes.
  * debian/patches/manpage-fix.patch: Update for the new domain.
  * debian/patches/ffmpeg-7.0.patch: New; avoid using a deprecated
    function to make life easier for the next ffmpeg transition.
  * debian/control (Build-Depends): Require libgnustep-gui-dev (>= 0.32)
    because of the above (in reality only gnustep-base/1.31 is needed).
    (Standards-Version): Claim compliance with 4.7.2 with no changes.

 -- Yavor Doganov <yavor@gnu.org>  Thu, 06 Mar 2025 23:20:23 +0200

lynkeos.app (3.7+dfsg-6) unstable; urgency=medium

  * Run wrap-and-sort -ast.
  * debian/control (Build-Depends): Require libgnustep-corebase-dev (>=
    0.1.1+20230710-5) and libgnustep-gui-dev (>= 0.31.1-7) for the
    multiarch layout.  Remove gnustep-make; implied.
  * debian/lynkeos.app.install: Update for multiarch (Closes: #1093992).
  * debian/rules (execute_before_dh_link): Remove conditional; unnecessary
    with current dh_gnustep.
  * debian/patches/gnustep-port.patch: Fix FTCBFS (Closes: #1093993).
  * debian/upstream/metadata: New file.
  * debian/copyright: Update copyright years.

 -- Yavor Doganov <yavor@gnu.org>  Sat, 01 Feb 2025 09:58:41 +0200

lynkeos.app (3.7+dfsg-5) unstable; urgency=medium

  * debian/control (Build-Depends): Add libgnustep-corebase-dev.
  * debian/patches/gnustep-port.patch: Remove workarounds for missing
    CoreFoundation functions.
  * debian/patches/gcc-warnings.patch: Refresh.

 -- Yavor Doganov <yavor@gnu.org>  Thu, 26 Sep 2024 16:42:41 +0300

lynkeos.app (3.7+dfsg-4) unstable; urgency=medium

  * debian/control (Build-Depends): Revert version requirement for ffmpeg
    libs now that the transition is done.
  * debian/rules (override_dh_auto_build): Pass --size 128 to icns2png to
    extract the right icon.

 -- Yavor Doganov <yavor@gnu.org>  Mon, 05 Aug 2024 13:59:35 +0300

lynkeos.app (3.7+dfsg-3) unstable; urgency=medium

  * debian/patches/gcc-14.patch: New; fix FTBFS with GCC 14 on 32-bit
    architectures, thanks Adrian Bunk for the report (Closes: #1077496).
  * debian/control (Build-Depends): Require ffmpeg libs (>= 7:7.0.1) to
    ensure the package gets rebuilt for the ongoing ffmpeg transition.
    Add gnustep-make (>= 2.9.2-2) for dh_gnustep's new functionality.
  * debian/rules (override_dh_auto_build): Pass --depth to icns2png in
    order to extract only one icon.
    (override_dh_link): Rename as...
    (execute_before_dh_link): ...and run dh_gnustep --app --move-to which
    simplifies the recipe a lot.  Move sed invocation to...
    (override_dh_fixperms-indep): ...this existing target.
  * debian/lynkeos.app.install: Install everything but Resources.  Install
    the app icon in /usr/share/pixmaps/GNUstep.
  * debian/Lynkeos.desktop: Use new icon location.
  * debian/watch: Use HTTPS protocol.

 -- Yavor Doganov <yavor@gnu.org>  Mon, 29 Jul 2024 17:14:47 +0300

lynkeos.app (3.7+dfsg-2) unstable; urgency=medium

  * debian/patches/gnustep-port.patch: Fix FTBFS with gnustep-gui/0.31.x
    (Closes: #1072736).
  * debian/control (Standards-Version): Bump to 4.7.0; no changes needed.

 -- Yavor Doganov <yavor@gnu.org>  Sat, 08 Jun 2024 10:55:36 +0300

lynkeos.app (3.7+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Adjust all patches for the new source layout.
  * debian/patches/gnustep-port.patch: Update for the new release and add
    new files BayerImageBuffer.m, FileSequenceWriter.m, FITSRawReader.m,
    geometry.c, LibRaw_Reader.m, LynkeosAlertPanel.m and SER_Writer.m.
  * debian/patches/gcc-warnings.patch: Refresh and fix more warnings.
  * debian/patches/glibc-2.27.patch: Refresh and fix more occurrences of
    the deprecated HUGE constant.
  * debian/patches/ffmpeg-5.0.patch: Remove hunks applied upstream.
  * debian/patches/ffmpeg-6.0.patch: Delete; fixed upstream.
  * debian/patches/nsbundle-exception.patch: New; fix typo in translations
    which causes an NSBundle exception while parsing the .strings files.
  * debian/rules: Update for the new source layout.
  * debian/control (Build-Depends): Add pkgconf and libraw-dev.
  * debian/copyright: Update all patterns as the layout has changed again.
    Update copyright years.

 -- Yavor Doganov <yavor@gnu.org>  Mon, 15 Jan 2024 19:32:58 +0200

lynkeos.app (3.5+dfsg-2) unstable; urgency=medium

  * debian/patches/ffmpeg-6.0.patch: New; fix FTBFS with ffmpeg/6.0
    (Closes: #1041402).  Thanks Sebastian Ramacher.

 -- Yavor Doganov <yavor@gnu.org>  Tue, 18 Jul 2023 17:15:29 +0300

lynkeos.app (3.5+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Adjust all patches for the new source layout.
  * debian/patches/gnustep-port.patch: Update and refresh.  Restore
    upstream XIBs.  Add new file SER_RAW_Reader.m to GNUstep/GNUmakefile.
    Remove alternative codepath for missing __builtin_shufflevector; it is
    available as of GCC 12.
  * debian/patches/manpage-fix.patch: Refresh.
  * debian/patches/gcc-warnings.patch: Likewise.
  * debian/patches/glibc-2.27.patch: Replace new occurrences of the
    deprecated HUGE constant.  Use HUGE_VAL instead of FLT_MAX.
  * debian/rules: Update for the new source layout.
  * debian/watch: Use repacksuffix w/o number; as advised by lintian.
  * debian/copyright: Update copyright years; update all patterns to match
    the new source layout.
  * debian/control (Standards-Version): Bump to 4.6.2; no changes needed.

 -- Yavor Doganov <yavor@gnu.org>  Sat, 04 Feb 2023 19:15:34 +0200

lynkeos.app (3.4+dfsg1-4) unstable; urgency=medium

  * debian/patches/ffmpeg-5.0.patch: New; fix FTBFS with ffmpeg/5.0
    (Closes: #1004766).
  * debian/patches/gnustep-port.patch: Fix incorrect block malloc size.
  * debian/patches/series: Update.
  * debian/copyright: Update copyright years.

 -- Yavor Doganov <yavor@gnu.org>  Sun, 13 Feb 2022 22:41:16 +0200

lynkeos.app (3.4+dfsg1-3) unstable; urgency=medium

  * debian/patches/gnustep-port.patch: Fix FTBFS with lapack/3.10.0
    (Closes: #997245).
  * debian/control (Build-Depends): Remove libgnustep-gui-dev version
    requirement; satisfied in bullseye.
    (Breaks, Replaces): Remove; obsolete.
    (Standards-Version): Bump to 4.6.0; no changes needed.
  * debian/lynkeos.app-common.lintian-overrides: Remove breakout-link,
    reportedly unused now.

 -- Yavor Doganov <yavor@gnu.org>  Wed, 27 Oct 2021 18:16:36 +0300

lynkeos.app (3.4+dfsg1-2) unstable; urgency=medium

  [ Peter Pentchev ]
  * debian/rules: Fix the arch-only build.

 -- Yavor Doganov <yavor@gnu.org>  Sat, 30 Jan 2021 17:59:17 +0200

lynkeos.app (3.4+dfsg1-1) unstable; urgency=medium

  * New upstream release.
  * debian/copyright: Update for new layout.  Update copyright years.
  * debian/patches/fix-spelling-error.patch: Update and refresh.
  * debian/patches/manpage-fix.patch: Likewise.
  * debian/patches/glibc-2.27.patch: Likewise.
  * debian/patches/gnustep-port.patch: Update for the new release, remove
    some NSURL workarounds that are now implemented, enable the image
    analyzer.  Retain the old XIBs as support for the new format is
    apparently still rough/unsatisfactory.
  * debian/patches/gcc-warnings.patch: Fix few more warnings.
  * debian/patches/ftbfs-ppc64el.patch: Update for the new layout.
  * debian/control (Build-Depends): Require libgnustep-gui-dev (>= 0.28)
    for the new NSURL APIs and modern XIB support.  Switch to the
    debhelper-compat mechanism; bump level to 13.
    (Standards-Version): Claim compliance with 4.5.1; no changes needed.
  * debian/compat: Delete.
  * debian/rules: Update for the new source layout.
    (marketver): New variable, extract MARKET_VERSION (sic).
    (override_dh_link-indep): Use it to set the value in the plist.
  * debian/source/include-binaries: Delete; no longer needed.
  * debian/lynkeos.app-common.lintian-overrides: Add breakout-link
    (symlink for FHS compliance) and
    package-contains-documentation-outside-usr-share-doc (RTF files loaded
    in the About dialog).

 -- Yavor Doganov <yavor@gnu.org>  Sun, 24 Jan 2021 20:29:35 +0200

lynkeos.app (3.3+dfsg1-2) unstable; urgency=medium

  * debian/control (Breaks, Replaces): Fix package name; thanks Andreas
    Beckmann (Closes: #946239).
  * debian/lynkeos.app.install: Add .desktop file.
  * debian/install: Delete; now redundant.
  * debian/rules (override_dh_link-indep): Delete stamp.make to make
    dh_missing happy.

 -- Yavor Doganov <yavor@gnu.org>  Fri, 06 Dec 2019 10:05:35 +0200

lynkeos.app (3.3+dfsg1-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches/fix-spelling-error.patch: Remove hunk moved to
    gnustep-port.path; refresh.
  * debian/patches/gnustep-port.patch: Update and refresh.
  * debian/patches/gcc-warnings.patch: Fix few more warnings; update for
    the new release and refresh.
  * debian/patches/glibc-2.27.patch: Refresh.
  * debian/rules: Extract current version from Xcode's project file and
    define it with DEB_CPPFLAGS_MAINT_APPEND.
    (DEB_LDFLAGS_MAINT_APPEND): Remove; unnecessary with GCC 9.
  * debian/compat: Set to 12.
  * debian/control (Build-Depends): Bump debhelper requirement.  Drop
    gnustep-make; required version is present in stable.  Remove version
    requirement for libgnustep-gui-dev, also present in stable.
    (lynkeos.app): Declare Breaks+Replaces because of the icon move.
    (lynkeos.app-common): Remove Replaces; update Breaks version.
    (Rules-Requires-Root): Set to no.
    (Standards-Version): Bump to 4.4.1; no changes needed.
  * debian/lynkeos.app-common.install: Move icon to lynkeos.app; AppStream
    requires the icon to be in the same package as the .desktop file.
  * debian/lynkeos.app.install: New file.
  * debian/copyright: Exclude "help" as the licensing terms are unclear
    and it's only used to build Apple help files after retrieving online
    wiki content.  We can't use it anyway and network operations during
    build are no-op.  Update copyright years.

 -- Yavor Doganov <yavor@gnu.org>  Mon, 25 Nov 2019 13:14:40 +0200

lynkeos.app (3.1+dfsg1-2) unstable; urgency=medium

  * debian/patches/ftbfs-ppc64el.patch: Update redefined types which were
    apparently missed by upstream for the __ALTIVEC__ branch.  Fixes yet
    another FTBFS on ppc64el.
  * debian/patches/gnustep-port.patch: Fix an honest typo which caused
    FTBFS on powerpc, powerpcspe and ppc64.

 -- Yavor Doganov <yavor@gnu.org>  Thu, 31 Jan 2019 13:24:22 +0200

lynkeos.app (3.1+dfsg1-1) unstable; urgency=medium

  * New upstream release.
  * Remove patches that are applied/fixed upstream:
    - libav-10.patch;
    - ffmpeg_2.9.patch;
    - ffmpeg-4.0.patch.
  * debian/patches/fix-spelling-error.patch: Fix another error; refresh.
  * debian/patches/gnustep-port.patch: Adapt for the current code:
    + Fix assertion failure with gnustep-base/1.26 (Closes: #920609).
    + Provide alternative for the Clang-specific __builtin_shufflevector.
    + Replace CoreFoundation functions with GNUstep Base equivalents.
    + Don't override NSCell methods; remove all SMDoubleSlider changes.
    + Build with dcraw/FITS/SER support; ignore the metadata as the
      implementation relies on CoreServices which has no free replacement.
    + Revert all XIBs to an old version as XIB files created by XCode 5
      are not supported by GNUstep yet.
  * debian/patches/gcc-warnings.patch: Remove hunks applied upstream; fix
    a few more warnings.  Check the return value of fread/fgets.
  * debian/patches/manpage-fix.patch: Refresh.
  * debian/patches/glibc-2.27.patch: Likewise.
  * debian/patches/series: Update.
  * debian/gbp.conf: New file.
  * debian/control (Build-Depends): Bump libgnustep-gui-dev to >= 0.27 for
    the new NSSliderCell implementation.  Drop libgnustep-base-dev;
    implied by -base.  Add libcfitsio-dev for the FITS reader/writer.
    Add liblapacke-dev for the new image alignment implementation.  Bump
    gnustep-make to >= 2.7.0-4 for "terse" support in DEB_BUILD_OPTIONS.
    (Depends): Add dcraw, required for reading the various RAW formats.
    (Standards-Version): Claim compliance with 4.3.0; no changes needed.
  * debian/rules (override_dh_auto_build): Support "terse".
    (override_dh_fixperms-indep): Amend Italian translations directory.
  * debian/copyright (Files-Excluded): Add SDK; just causes lintian
    warning P: source-contains-empty-directory.
    Remove copyright holders for patches that are removed.  Correct
    location of translations; update copyright years.

 -- Yavor Doganov <yavor@gnu.org>  Thu, 31 Jan 2019 00:06:43 +0200

lynkeos.app (2.10+dfsg1-3) unstable; urgency=medium

  * debian/patches/ftbfs-ppc64el.patch: New, fix AltiVec-related FTBFS on
    ppc64el (Closes: #893442).  Many thanks to Frédéric Bonnard.
  * debian/patches/series: Update.
  * debian/copyright: Add Frédéric.

 -- Yavor Doganov <yavor@gnu.org>  Sun, 25 Mar 2018 15:26:01 +0300

lynkeos.app (2.10+dfsg1-2) unstable; urgency=low

  * debian/patches/gnustep-port.patch: Fix FTBFS on most architectures
    (undeclared PAGE_SIZE) (Closes: #893097).  Avoid locking the low knob
    of the double slider when it is set to the minimal value.
  * debian/control (lynkeos.app-common): Mark as M-A: foreign.

 -- Yavor Doganov <yavor@gnu.org>  Fri, 16 Mar 2018 16:04:26 +0200

lynkeos.app (2.10+dfsg1-1) unstable; urgency=low

  * New upstream release (Closes: #483430).
  * Set urgency to "low" for a longer testing period.
  * debian/patches/gnustep-port.patch: New; port to GNUstep.
  * debian/patches/ffmpeg-4.0.patch: New; fix FTBFS with ffmpeg/4.0
    (Closes: #888339).  Thanks James Cowgill for the report.  While here,
    move away from the legacy decode API; the new API works better with
    some codecs and hopefully the switch would lead to smoother ffmpeg
    transitions in the future.
  * debian/patches/glibc-2.27.patch: New; fix FTBFS with glibc/2.27
    (Closes: #891336).  Thanks Aurelien Jarno for the report.
  * debian/patches/libav-10.patch: Remove hunks applied upstream, update
    for the current code.
  * debian/patches/ffmpeg_2.9.patch: Adapt for current code.
  * debian/patches/gcc-warnings.patch: Remove all hunks as they were
    either fixed upstream or no longer apply to current code; fix some new
    warnings.
  * debian/patches/manpage-fix.patch: Write a new manpage and get it
    installed by the upstream build system.
  * debian/patches/fix-spelling-error.patch: Remove irrelevant hunk, fix
    one new spelling error.
  * debian/patches/hurd-ftbfs-fix.patch:
  * debian/patches/libav-build-fix.patch:
  * debian/patches/libav-0.7.patch:
  * debian/patches/libav-9.patch: Remove; fixed upstream.
  * debian/patches/compilation-errors.patch:
  * debian/patches/format-security.patch:
  * debian/patches/plist-icon.patch: Remove; no longer applicable.
  * debian/patches/series: Update.
  * debian/compat: Bump to 11.
  * debian/menu: Delete.
  * debian/install: Don't install the .xpm icon.  Install arch-dep files.
  * debian/lynkeos.app-common.install: New file.
  * debian/control (Build-Depends): Remove imagemagick.  Bump gnustep-make
    dependency to >= 2.7.0-3 for the optim definition.  Add icnsutils.
    Bump debhelper to >= 11.  Remove ancient version requirements for
    ffmpeg libraries.  Require latest GNUstep libraries which contain
    fixes related to this release.
    (lynkeos.app-common): New package, split arch-indep files.
    (Depends): Remove ${gnustep:Depends}; obsolete.  Depend on -common.
    (Vcs-Git, Vcs-Browser): Update following the migration to Salsa.
    (Standards-Version): Claim compliance with 4.1.3 as of this release.
  * debian/rules: Pass --sourcedirectory=GNUstep to dh.  Don't define
    optim.  Enable all hardening.
    (d_com): New helper variable.  Redefine d_app accordingly.
    (override_dh_auto_build): Don't create the .xpm icon.  Don't create
    symlink for the manpage.  Extract a .png icon from upstream .icns.
    Replace $(MAKE) with dh_auto_build.
    (override_dh_auto_install, override_dh_clean)
    (override_dh_installchangelogs): Remove.
    (override_dh_link): Rename as -indep and adjust recipe for the
    arch:all package.  Don't invoke gsdh_gnustep; obsolete.
    (override_dh_fixperms): Rename as -indep; fix permissions for Italian
    translation files.
  * debian/Lynkeos.png:
  * debian/manpages: Delete, no longer necessary.
  * debian/Lynkeos.desktop: Adjust Icon field.
  * debian/clean: New file; clean the .png icon.
  * debian/changelog: Remove trailing whitespace.
  * debian/watch: Uncomment, add repacking options.
  * debian/copyright: Use Files-Excluded to repackage the upstream tarball
    due to a non-free image.  Update copyright years, add copyright
    holders for translations.  Add copyright/license for SMDoubleSlider.
    Use HTTPS for the Format URL.
  * debian/lynkeos.app-common.lintian-overrides: New file, override
    extra-license-file as these are loaded in the About dialog.

 -- Yavor Doganov <yavor@gnu.org>  Sat, 10 Mar 2018 21:29:04 +0200

lynkeos.app (1.2-7.1) unstable; urgency=medium

  * Non-maintainer upload.

  [ Andreas Cadhalpun ]
  * Fix build with ffmpeg 3.0. (Closes: #803840)

 -- Sebastian Ramacher <sramacher@debian.org>  Sun, 06 Mar 2016 22:46:15 +0100

lynkeos.app (1.2-7) unstable; urgency=medium

  * Ack NMUs, thanks Sebastian Ramacher and Moritz Muehlenhoff.
  * debian/source/format: Switch to 3.0 (quilt).
  * debian/compat: Set to 9.
  * debian/control (Build-Depends): Remove dpatch and openssl.  Bump
    debhelper and libav version requirement.  Add gnustep-make (>=
    2.6.6-2).
    (Description): Improve.
    (Vcs-Git, Vcs-Browser): Use the canonical URIs.
    (Homepage): Switch to sf.net as the page of the GS port gives 404.
    (Standards-Version): Compliant with 3.9.6 as of this release.
  * debian/rules: Rewrite for modern dh.
  * debian/patches/00list:
  * debian/patches/05_ffmpeg_build_fix.dpatch:
  * debian/patches/10_plist_icon.dpatch:
  * debian/patches/15_manpage.dpatch:
  * debian/patches/compilation-errors.dpatch:
  * debian/patches/libav_0.7.dpatch:
  * debian/patches/libav_9.dpatch:
  * debian/patches/libav_10.dpatch: Rename/quiltify as...
  * debian/patches/series:
  * debian/patches/libav-build-fix.patch:
  * debian/patches/plist-icon.patch:
  * debian/patches/manpage-fix.patch:
  * debian/patches/compilation-errors.patch:
  * debian/patches/libav-0.7.patch:
  * debian/patches/libav-9.patch:
  * debian/patches/libav-10.patch: ...and make them DEP-3 compliant.
  * debian/patches/format-security.patch: New, fixes FTBFS with the
    default buildflags.  Thanks Colin Watson (Closes: #645993).
  * debian/patches/hurd-ftbfs-fix.patch: New, fixes FTBFS on GNU/Hurd.
    Thanks Pino Toscano (Closes: #670050).
  * debian/patches/gcc-warnings.patch: New; fixes implicit function
    declarations and few other issues (Closes: #749760).
  * debian/patches/libav-10.patch: Replace the deprecated function
    avcodec_alloc_frame with av_frame_alloc.
  * debian/patches/fix-spelling-error.patch: New, thanks Paul Gevers.
  * debian/patches/manpage-fix.patch: Typo fix.  Install into a valid
    section, thanks Paul Gevers.
  * debian/source/include-binaries:
  * debian/install:
  * debian/manpages: New files.
  * debian/Lynkeos.png: New file; moved/generated from plist-icon.patch.
  * debian/README.source:
  * debian/preinst: Delete.
  * debian/Lynkeos.desktop: Add Keywords field.
  * debian/copyright: Switch to format 1.0.

 -- Yavor Doganov <yavor@gnu.org>  Mon, 20 Oct 2014 19:49:09 +0300

lynkeos.app (1.2-6.2) unstable; urgency=medium

  * Non-maintainer upload.
  * debian/patches/libav_10.dpatch: Port to libav 10 API. Thanks to Anton
    Khirnov for the patch (Closes: #739316)
  * Build-depend on libtiff-dev (Closes: #722968)

 -- Moritz Muehlenhoff <jmm@debian.org>  Tue, 11 Mar 2014 20:13:54 +0100

lynkeos.app (1.2-6.1) unstable; urgency=low

  * Non-maintainer upload.
  * debian/patches/libav_9.dpatch: Port to libav 9 API. Thanks to Moritz
    Muehlenhoff for the patch (Closes: #720820)

 -- Sebastian Ramacher <sramacher@debian.org>  Wed, 04 Sep 2013 14:12:18 +0200

lynkeos.app (1.2-6) unstable; urgency=low

  * debian/patches/compilation-errors.dpatch: New (Closes: #629206).
  * debian/patches/libav_0.7.dpatch: New; port to libav/0.7, thanks Colin
    Watson (Closes: #638535).
  * debian/patches/00list: Update.
  * debian/control (Build-Depends): Add libtiff4-dev.  Require
    libavcodec-dev/libavformat-dev/libswscale-dev >= 4:0.7.1.
    (Vcs-Arch): Replace with...
    (Vcs-Git, Vcs-Browser): ...as the package is now maintained in Git.
    (Standards-Version): Bump to 3.9.2; no changes required.

 -- Yavor Doganov <yavor@gnu.org>  Mon, 26 Sep 2011 19:06:08 +0300

lynkeos.app (1.2-5) unstable; urgency=low

  * debian/compat: Set to 7.
  * debian/control (Section): Change to `gnustep'.
    (Build-Depends): Require debhelper >= 7, remove version constraint for
    libgnustep-gui-dev.  Drop libdts-dev.
    (Depends): Add ${gnustep:Depends}.
    (Standards-Version): Compliant with 3.8.4 as of this release.
  * debian/rules: Include /usr/share/GNUstep/debian/config.mk, export
    GNUSTEP_MAKEFILES and avoid gs_make.
    (d_app): New variable.
    (OPTFLAG, ADDITIONAL_CFLAGS): No longer define; rework noopt handling
    to be compatible with gnustep-make/2.4.x (Closes: #581957).
    (install): Replace dh_clean -k with dh_prep.
    (binary-arch): Don't install the lintian override.  Conditionally move
    Resources to /usr/share.
  * debian/preinst:
  * debian/source/format:
  * debian/README.source: New file.
  * debian/lintian-override: Delete.
  * debian/Lynkeos.desktop: Make it valid.
  * debian/patches/05_ffmpeg_build_fix.dpatch: Add -ltiff to
    ADDITIONAL_OBJC_LIBS; fixes FTBFS with GNU gold (Closes: #555578).
    Remove -ldts; not needed.

 -- Yavor Doganov <yavor@gnu.org>  Sun, 30 May 2010 16:58:57 +0300

lynkeos.app (1.2-4) unstable; urgency=low

  * debian/compat: Set to 6.
  * debian/control (Build-Depends): Bump debhelper to >= 6.  Version the
    dependency on libavcodec-dev to ensure that the package gets built
    against libavcodec52 on all architectures.  Add libswscale-dev.
    (Vcs-Arch): New field.
  * debian/patches/05_ffmpeg_build_fix.dpatch: Update for the current
    ffmpeg:
    + Don't set ADDITIONAL_INCLUDE_DIRS.
    + Add -lswscale to ADDITIONAL_OBJC_LIBS.
    + Include the correct headers (Closes: #518233).
    + Replace the deprecated `img_convert' function with `sws_scale'
    (Closes: #487641).
  * debian/watch: New file, currently with commented pattern (Addresses:
    #483430).

 -- Yavor Doganov <yavor@gnu.org>  Wed, 11 Mar 2009 15:14:08 +0200

lynkeos.app (1.2-3) unstable; urgency=low

  * debian/patches/05_ffmpeg_build_fix.dpatch: Remove lots of useless
    libraries from ADDITIONAL_OBJC_LIBS.  This fixes FTBFS (Closes:
    #489845) and maybe will make the Ubuntu maintainer's life easier
    (Closes: #455771).
  * debian/control (Build-Depends): Wrap them and bump libgnustep-gui-dev
    to >= 0.14.
    (Standards-Version): Set to 3.8.0; no changes required.
  * debian/rules: Use default and automatic variables where possible.
    (ADDITIONAL_OBJCFLAGS): Rename as...
    (OPTFLAG): ...as it is used only for the optimization flag.

 -- Yavor Doganov <yavor@gnu.org>  Wed, 09 Jul 2008 19:07:22 +0300

lynkeos.app (1.2-2) unstable; urgency=low

  * debian/control (Maintainer): Adopting package, set the GNUstep team as
    maintainer (Closes: #448739).
    (Uploaders): Add myself.
    (Section): Changed to `science'.
    (Build-Depends): Bump debhelper to (>= 5), libgnustep-gui-dev to (>=
    0.12).  Replace fftw3-dev with libfftw3-dev.  Add dpatch and
    imagemagick.  Drop gnustep-make.
    (Standards-Version): Bump to 3.7.2 (no changes).
    (Homepage): Moved to its own field.
  * Acknowledge NMUs, many thanks to Julien Danjou and Hubert Chan
    (Closes: #370268, #391260).
  * debian/compat: Bump level to 5.
  * debian/dirs: Deleted.
  * debian/manpages: Likewise.
  * debian/menu (section): Changed to `Applications/Science/Astronomy'.
    (longtitle, icon): Added.
  * debian/rules: Include include /usr/share/dpatch/dpatch.make.
    (CFLAGS): Do not define.
    (ADDITIONAL_OBJCFLAGS, ADDITIONAL_CFLAGS): New conditionally defined
    variables.
    (configure, configure-stamp): Delete these targets.
    (build): Depend on the `patch' target.
    (build-stamp): Remove `configure-stamp' from prerequisites.  Use
    gs_make and do not source GNUstep.sh (Closes: #451445).  Honor
    DEB_BUILD_OPTIONS.  Generate the PNG icon and convert it to XPM.
    (clean): Renamed to `clean-patched'; depend on it and `unpatch'.
    (clean-patched): Do not delete `configure-stamp' as it is not
    created.  Do not ignore errors.  Delete the XPM icon and the manpage
    symlink as well.
    (install): Use gs_make, the standard DESTDIR variable and
    GNUSTEP_INSTALLATION_DOMAIN.  Remove the invalid .desktop file and
    install ours.  Do not invoke dh_installdirs.  Remove the executable
    bits of ImageListWindow.gorm.
    (binary-arch): Do not create the app wrapper.  Remove needless
    dh_installexamples invocation.  Use gsdh_gnustep.  Create a symlink
    for the manpage (fixes lintian warning).
    (.PHONY): Drop configure.
  * debian/Lynkeos.desktop: Added.
  * Split the patches from diff.gz in debian/patches and maintain them
    with dpatch:
    + 05_ffmpeg_build.dpatch.
    + 10_plist_icon.dpatch.
    + 15_manpage.dpatch: New.
  * debian/patched/00list: Create.

 -- Yavor Doganov <yavor@gnu.org>  Sat, 17 Nov 2007 16:29:00 +0200

lynkeos.app (1.2-1.2) unstable; urgency=low

  * Non-maintainer upload
  * Rebuild against new GNUstep libraries.
    - Update Build-dep to libgnustep-gui-dev.
    - Fix problem with NSMovie aliasing.
  * Add license header to debian/copyright file.

 -- Hubert Chan <hubert@uhoreg.ca>  Thu,  5 Oct 2006 11:07:58 -0400

lynkeos.app (1.2-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix FTBFS
    - Build-dep on libgnustep-gui0.10-dev instead of libgnustep-gui0.9-dev
      (Closes: #350429)
    - Fix problem with libavcodec-dev (Closes: #327429)
    - Add an include path to Sources/GNUmakefile

 -- Julien Danjou <acid@debian.org>  Thu,  1 Jun 2006 11:46:49 +0200

lynkeos.app (1.2-1) unstable; urgency=low

  * Initial Release. (closes: #300000)

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Sun, 17 Apr 2005 21:35:42 +0200
