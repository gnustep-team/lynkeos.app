//
//  Lynkeos
//  SER_Writer.h
//
//  Created by Jean-Etienne LAMIAUD on Wed Dec 20 2023.
//  Copyright (c) 2023. Jean-Etienne LAMIAUD
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//

#ifndef __SER_Writer_h
#define __SER_Writer_h

#import <Foundation/Foundation.h>

#include "LynkeosFileWriter.h"

#include "SER.h"

NS_ASSUME_NONNULL_BEGIN

@interface SER_Writer : NSObject <LynkeosMovieFileWriter>
{
@private
   u_short  _nPlanes;        //!< Number of planes being saved
}
@end

NS_ASSUME_NONNULL_END

#endif // __SER_Writer_h
