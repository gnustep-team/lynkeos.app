//
//  Lynkeos
//  $Id: $
//
//  Created by Jean-Etienne LAMIAUD on Tue Feb 14 2023.
//  Copyright (c) 2023. Jean-Etienne LAMIAUD
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//

/*!
 * @header
 * @abstract Reader for RAW file format, using libraw.
 */
#ifndef __LIBRAWREADER_H
#define __LIBRAWREADER_H

#include "LynkeosFileReader.h"

#include "BayerImageBuffer.h"

/*!
 * @abstract Class for reading Raw image file format.
 * @ingroup FileAccess
 */
@interface LibRaw_Reader : NSObject <LynkeosImageFileReader, LynkeosCustomImageFileReader>
{
@private
   NSURL               *_url;            //!< RAW file path
   u_short              _width;          //!< Useful sensor area width
   u_short              _bytesPerRow;    //!< RAW line length in bytes
   u_short              _height;         //!< Useful sensor area height
   u_short              _xOrigin;        //!< Useful sensor area x origin
   u_short              _yOrigin;        //!< Useful sensor area y origin
   u_short              _xLimit;         //!< Useful sensor area x limit
   u_short              _yLimit;         //!< Useful sensor area y limit
   double               _scale;          //!< Scale to put file format maximum value to 255
   double               _whiteBalance[3]; //!< Color weights
   ListMode_t           _mode;           //!< Current mode, taken into account in conversion
   BayerImageBuffer    *_darkFrame;      //!< Dark frame in same bayer format
   LynkeosImageBuffer  *_flatField;      //!< Flat field (in planar format)
   NSMutableDictionary *_metadata;       //!< Movie metadata
}

@end

#endif
