//
//  Lynkeos
//  $Id$
//
//  Created by Jean-Etienne LAMIAUD on Fri Jun 23 2023.
//  Copyright (c) 2023. Jean-Etienne LAMIAUD
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//

/*!
 * @header
 * @abstract Definitions for the gamma correcter utility class.
 */

#ifndef geometry_h
#define geometry_h

#include <CoreFoundation/CoreFoundation.h>

bool pointIsInsideRect(CGPoint point, CGRect rect);
bool segmentIntersectsHorizontalSegment(CGPoint start, CGPoint end, CGFloat xStart, CGFloat xEnd, CGFloat y);
bool segmentIntersectsVerticalSegment(CGPoint start, CGPoint end, CGFloat yStart, CGFloat yEnd, CGFloat x);
bool segmentIntersectsRect(CGPoint start, CGPoint end, CGRect rect);

#endif /* geometry_h */
