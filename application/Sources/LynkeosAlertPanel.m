//
//  Lynkeos
//  $Id$
//
//  Created by Jean-Etienne LAMIAUD on Sat Aug 5 2023.
//  Copyright (c) 2023. Jean-Etienne LAMIAUD
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//

#include "LynkeosCore/LynkeosAlertPanel.h"

@implementation LynkeosAlertPanel
+ (NSModalResponse) runAlertWithTitle:(NSString*)title style:(NSAlertStyle)style message:(NSString*)message
{
   LynkeosAlertPanel *panel= [[[self alloc] init] autorelease];

   if (panel != nil)
   {
      panel.alertStyle = style;
      panel.messageText = title;
      [panel layout];
      NSRect r = [panel.window contentLayoutRect];
      NSText *infoText = [[[NSText alloc] initWithFrame:NSMakeRect(0.0, 0.0, r.size.width, 50.0)]
                          autorelease];
      infoText.string = message;
      infoText.alignment = NSTextAlignmentJustified;
      infoText.backgroundColor = NSColor.clearColor;
      panel.accessoryView = infoText;
      return [panel runModal];
   }
   else
      return NSModalResponseAbort;
}
@end
