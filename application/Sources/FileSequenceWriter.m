//
//  Lynkeos
//  $Id$
//
//  Created by Jean-Etienne LAMIAUD on Sat Aug 5 2023.
//  Copyright (c) 2023. Jean-Etienne LAMIAUD
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//

#include <tiffio.h>

#include <AppKit/AppKit.h>

#include "FileSequenceWriter.h"

@implementation FileSequenceWriter

+ (void) load
{
   // Nothing to do, this is just to force the runtime to load this class
}

- (id) init
{
   if ( (self = [super init]) != nil )
   {
      _tiffWriter = nil;
   }
   
   return( self );
}

+ (NSString*) writerName { return( @"File sequence" ); }

+ (NSString*) fileExtension { return( @"tiff" ); }

+ (BOOL) canSaveDataWithPlanes:(u_short)nPlanes
                         width:(u_short)w height:(u_short)h
                      metaData:(NSDictionary*)metaData
{
   return( [MyTiffWriter canSaveDataWithPlanes:nPlanes
                                         width:w
                                        height:h
                                      metaData:metaData] );
}

- (NSPanel*) configurationPanel
{
   if (_tiffWriter != nil)
      return [_tiffWriter configurationPanel];
   else
      return nil;
}

+ (id <LynkeosFileWriter>) writerForURL:(NSURL*)url
                                 planes:(u_short)nPlanes
                                  width:(u_short)w height:(u_short)h
                               metaData:(NSDictionary*)metaData
{
   // No pre-processing needed
   FileSequenceWriter *writer = [[[self alloc] init] autorelease];
   writer->_tiffWriter = (MyTiffWriter*)[[MyTiffWriter writerForURL:url   // TiffWriter does not use url here
                                                             planes:nPlanes
                                                              width:w
                                                             height:h
                                                           metaData:metaData] retain];

   return writer;
}

- (u_short) numberOfPlanes {return 3;}

- (void) saveMovieAtURL:(NSURL *)url
           withDelegate:(id<LynkeosMovieFileWriterDelegate>)delegate
                 opaque:(void *)opaque
             blackLevel:(double)black
             whiteLevel:(double)white
             withPlanes:(u_short)nPlanes
                  width:(u_short)w
                 height:(u_short)h
               metaData:(NSDictionary *)metaData
{
   const REAL * const * planes;
   u_short lineW;
   int index = 0;

   for (;;)
   {
      BOOL canceled;
      [delegate getNextFrameWithData:&planes lineWidth:&lineW opaque:opaque canceled:&canceled];
      if (canceled || planes == NULL)
         return;

      NSString *path = [url path];
      path = [path stringByDeletingPathExtension];
      path = [path stringByAppendingFormat:@"-%04d.", index];
      path = [path stringByAppendingString:[url pathExtension]];
      [_tiffWriter saveImageAtURL:[NSURL fileURLWithPath:path]
                         withData:planes
                       blackLevel:black whiteLevel:white
                       withPlanes:nPlanes width:w lineWidth:lineW height:h
                         metaData:metaData];

      index++;
   }
}

@end
