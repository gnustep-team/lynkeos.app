//
//  Lynkeos
//  $Id: $
//
//  Created by Jean-Etienne LAMIAUD on Thu Feb 14 2023.
//  Copyright (c) 2023. Jean-Etienne LAMIAUD
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//

/*!
 * @header
 * @abstract Bayer image support
 */

#ifndef __BayerImageBuffer_h
#define __BayerImageBuffer_h

#include "LynkeosImageBuffer.h"

NS_ASSUME_NONNULL_BEGIN

@interface BayerImageBuffer : LynkeosImageBuffer
{
   @public
   LynkeosImageBuffer *_weight;
   double              _accumulations;
   BOOL                _isProcessedDark;
}

/*!
 * @abstract Initialise a bayer image buffer from raw bayer data
 * @param data Raw bayer data
 * @param opaque Opaque data to be passed back to the getPlane function
 * @param getPlane Function returning the color plane of the x, y data pixel
 * @param width Data pixels width
 * @param lineW Data line pixels width
 * @param height Data pixels height
 * @param xoffset X offset of the data in the sensor image
 * @param yoffset Y offset of the data in the sensor image
 * @param x X origin, in the sensor, image of the resulting bayer image
 * @param y Y origin, in the sensor image, of the resulting bayer image
 * @param w Width of the resulting bayer image
 * @param h Height of the resulting bayer image
 * @param transform Affine transfomr applied to the bayer image coordinates before data sampling
 * @param colorAlign Sub pixel offset for each color plane
 * @param dark Dark bayer image for calibration
 * @param flat Flat bayer image for calibration
 */
- (id) initWithData:(REAL*)data
              opaque:(nullable void*)opaque
           getPlane:(int (*)(void *opaque, u_short x, u_short y))getPlane
              width:(u_short)width lineW:(u_short)lineW height:(u_short)height
            xoffset:(u_short)xoffset yoffset:(u_short)yoffset
                atX:(u_short)x Y:(u_short)y W:(u_short)w H:(u_short)h
      withTransform:(NSAffineTransformStruct)transform
     withColorAlign:(const NSPoint*)colorAlign
           withDark:(BayerImageBuffer*)dark withFlat:(LynkeosImageBuffer*)flat;
@end

NS_ASSUME_NONNULL_END

#endif
