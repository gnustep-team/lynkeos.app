//
//  Lynkeos
//  $Id$
//
//  Created by Jean-Etienne LAMIAUD on Tue Feb 28 2023.
//  Copyright (c) 2023. Jean-Etienne LAMIAUD
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//

/*!
 * @header
 * @abstract Reader for RAW FITS images
 */
#ifndef __FITSRAWREADER_H
#define __FITSRAWREADER_H

#include <fitsio.h>

#include "BayerImageBuffer.h"

#include "LynkeosFileReader.h"

/*!
 * @abstract Class for reading RAW FITS image file format.
 * @ingroup FileAccess
 */
@interface FITSRawReader : NSObject <LynkeosImageFileReader, LynkeosCustomImageFileReader>
{
@private
   fitsfile    *_fits;        //!< CFITSIO handle on the FITS file
   u_short     _width;        //!< Cached width
   u_short     _height;       //!< Cached height
   double      _imageScale;   //!< Value scale of image
   double      _imageZero;    //!< Zero value of image
   double      _minValue;     //!< Minimum value of data
   double      _maxValue;     //!< Maximum value of data
   BOOL        _bottomUpRows; //!< Row order
   u_short    _bayerArray[2][2]; //!< Bayer pattern
   ListMode_t           _mode;           //!< Current mode, taken into account in conversion
   BayerImageBuffer    *_darkFrame;      //!< Dark frame in same bayer format
   LynkeosImageBuffer  *_flatField;      //!< Flat field (in planar format)
   NSMutableDictionary *_metadata;       //!< Movie metadata
}

@end

#endif
